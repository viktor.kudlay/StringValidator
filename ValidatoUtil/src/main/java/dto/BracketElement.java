package dto;

/**
 * Created by vikto on 05.10.2016.
 */
public class BracketElement {
    private Character element;
    private Integer position;

    public BracketElement(Character element, Integer position) {
        this.element = element;
        this.position = position;
    }

    public Character getElement() {
        return element;
    }

    public void setElement(Character element) {
        this.element = element;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "\n{" +
                "el:\"" + element +
                "\", position:" + position +
                "}";
    }
}
