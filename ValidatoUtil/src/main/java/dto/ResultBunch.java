package dto;

import java.util.List;

/**
 * Created by vikto on 05.10.2016.
 */
public class ResultBunch {
    private boolean valid;
    private List<BracketElement> brackets;

    public ResultBunch(boolean valid) {
        this.valid = valid;
    }

    public ResultBunch(boolean valid, List<BracketElement> brackets) {
        this.valid = valid;
        this.brackets = brackets;
    }

    public List<BracketElement> getBrackets() {
        return brackets;
    }

    public void setBrackets(List<BracketElement> brackets) {
        this.brackets = brackets;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
