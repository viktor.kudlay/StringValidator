package dto;

/**
 * Created by ghostcky on 04.10.16.
 */
public class СharactersBunch {
    private Character opened;
    private Character closed;

    public СharactersBunch(Character opened, Character closed) {
        this.opened = opened;
        this.closed = closed;
    }

    public Character getOpened() {
        return opened;
    }

    public void setOpened(Character opened) {
        this.opened = opened;
    }

    public Character getClosed() {
        return closed;
    }

    public void setClosed(Character closed) {
        this.closed = closed;
    }
}
