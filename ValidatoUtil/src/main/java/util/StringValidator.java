package util;

import dto.BracketElement;
import dto.ResultBunch;
import dto.СharactersBunch;

import java.util.*;

/**
 * Created by ghostcky on 04.10.16.
 */
public class StringValidator {
    private Map<Character, СharactersBunch> bunch = null;

    /**
     * Инициализация валидатора
     *
     * @param bunchArray Массив Открывающих/Закрывающих тегов используемых в проверки валидности
     */
    public StringValidator(СharactersBunch... bunchArray) {
        bunch = new HashMap<>();
        for (СharactersBunch b : bunchArray)
            put(b);
    }

    /**
     * Добавление нового элемента в массив валидных значений
     *
     * @param o Открывающийся/Закрывающийся тег
     */
    public void put(СharactersBunch o) {
        bunch.put(o.getOpened(), o);
        bunch.put(o.getClosed(), o);
    }

    /**
     * Проверка на доступность тега в массиве валидных тегов
     *
     * @param c Тег используемый для поиска
     * @return признак присутствия тега
     */
    public boolean contains(Character c) {
        return bunch.containsKey(c);
    }

    /**
     * Проверка на доступность связки открывающийся/закрывающийся тег в массиве валидных тегов
     * (Используется для расширения использования)
     *
     * @param c Открывающийся/закрывающийся тег используемый для поиска
     * @return Признак присутствия тега
     */
    public boolean contains(СharactersBunch c) {
        return bunch.containsValue(c);
    }

    /**
     * Получить связку открывающийся/закрывающийся тег по одному из них (Используется для расширения использования)
     *
     * @param c Открывающийся или закрывающийся тег в связке
     * @return Открывающийся/закрывающийся тег
     */
    public СharactersBunch get(Character c) {
        return bunch.get(c);
    }

    /**
     * Удалить связку открывающийся/закрывающийся тег по одному из них (Используется для расширения использования)
     *
     * @param c Открывающийся или закрывающийся тег в связке
     * @return Найденный открывающийся/закрывающийся тег
     */
    public СharactersBunch remove(Character c) {
        return bunch.remove(c);
    }

    /**
     * Удалить связку открывающийся/закрывающийся тег по связке (Используется для расширения использования)
     *
     * @param c Связку открывающийся/закрывающийся тег
     * @return true - удалено, false - Объект не найден
     */
    public boolean remove(СharactersBunch c) {
        List<СharactersBunch> arr = new ArrayList<>();
        if (contains(c.getOpened()))
            arr.add(bunch.remove(c.getOpened()));
        if (contains(c.getClosed()))
            arr.add(bunch.remove(c.getClosed()));
        arr.add(bunch.remove(c.getClosed()));
        return arr.size() != 0;
    }

    /**
     * Проверка строки на валидность по валидным тегам, в тексте отсутствуют сторонние теги,
     * кроме описанных в массиве валидности (Используется для расширения использования)
     *
     * @param str        Строка для проверки на валидность
     * @param bunchArray Массив Открывающих/Закрывающих тегов используемых в проверки валидности
     * @return Признак валидности строки
     * @throws NullPointerException Входящее значение не может быть пустым
     * @throws NoSuchFieldException Передана строка с некоректным значением тега (Отличным от Массива валидных)
     */
    public static ResultBunch validate(String str, СharactersBunch... bunchArray) throws NullPointerException,
            NoSuchFieldException {
        return new StringValidator(bunchArray).validate(str, false);
    }

    /**
     * Проверка строки на валидность по валидным тегам (Используется для расширения использования)
     *
     * @param str        Строка для проверки на валидность
     * @param otherChars Имеет сторонние, не рассматриваемые символы в тексте, отличные массива тегов используемых
     *                   в проверки валидности
     * @param bunchArray Массив Открывающих/Закрывающих тегов используемых в проверки валидности
     * @return Результирующий обект корректности строки
     * @throws NullPointerException Входящее значение не может быть пустым
     * @throws NoSuchFieldException Передана строка с некоректным значением тега (Отличным от Массива валидных)
     */
    public static ResultBunch validate(String str, boolean otherChars, СharactersBunch... bunchArray) throws NullPointerException,
            NoSuchFieldException {
        return new StringValidator(bunchArray).validate(str, otherChars);
    }

    /**
     * Проверка строки на валидность по валидным тегам, в тексте отсутствуют сторонние теги,
     * кроме описанных в массиве валидности(Используется для расширения использования)
     *
     * @param str Строка для проверки на валидность
     * @return Результирующий обект корректности строки
     * @throws NullPointerException Входящее значение не может быть пустым
     * @throws NoSuchFieldException Передана строка с некоректным значением тега (Отличным от Массива валидных)
     */
    public ResultBunch validate(String str) throws NullPointerException, NoSuchFieldException {
        return validate(str, false);
    }

    /**
     * Проверка строки на валидность по валидным тегам
     *
     * @param str        Строка для проверки на валидность
     * @param otherChars Имеет сторонние, не рассматриваемые символы в тексте, отличные массива тегов используемых
     *                   в проверки валидности
     * @return Результирующий обект корректности строки
     * @throws NullPointerException Входящее значение не может быть пустым
     * @throws NoSuchFieldException Передана строка с некоректным значением тега (Отличным от Массива валидных)
     */
    public ResultBunch validate(String str, boolean otherChars) throws NullPointerException, NoSuchFieldException {
        if(!checkString(str))
            return new ResultBunch(false);

        int i = -1;

        Stack<BracketElement> stack = new Stack<>();
        List<BracketElement> bracketElements = new ArrayList<>();

        for (char c : str.toCharArray()) {
            i++;

            if (!contains(c)) { // Если элемент не найден в массиве тегов
                if (otherChars) // и не выставлен флаг о пропуске подобных элементов
                    continue;
                System.out.println("Incorrect character field: " + c);
                return new ResultBunch(false); // throw new NoSuchFieldException("Incorrect character field: " + c); или если должны проверить ошибку
            }

            СharactersBunch curButch = bunch.get(c);

            if (!curButch.getClosed().equals(c)) {
                BracketElement el = new BracketElement(c, i);
                stack.add(el);
                bracketElements.add(el);
            }
            else {
                if(stack.size() == 0){
                    System.out.println("Has not opened symbol");
                    return new ResultBunch(false);
                }

                BracketElement sBracket = stack.pop();

                if (!sBracket.getElement().equals(curButch.getOpened())) {
                    СharactersBunch character = bunch.get(sBracket.getElement());

                    System.out.println("Incorrect closed character field: \"" + c + "\", expected: \"" +
                            character.getClosed() + "\", position: " + i);
                    return new ResultBunch(false);
                }

                bracketElements.add(new BracketElement(c, i));
            }
        }
        if (stack.size() != 0) {
            System.out.println("Has not closed symbols");
            return new ResultBunch(false);
        }

        return new ResultBunch(true, bracketElements);
    }

    private boolean checkString(String str){
        if (str == null || str.isEmpty()) {
            System.out.println("Входящее значение не может быть пустым");
            return false; // throw new NullPointerException("Входящее значение не может быть пустым"); или если должны проверить ошибку
        }

        boolean isValid = false;

        for(Map.Entry<Character, СharactersBunch> e: bunch.entrySet()){ // Проверка на доступность в строке хотябы одной пары закрывающего/открывающего тега
            if(e.getKey().equals(e.getValue().getOpened())) {
                if (str.contains(e.getValue().getOpened().toString()) && str.contains(e.getValue().getClosed().toString())) {
                    isValid = true;
                    break;
                }
            }
        }

        if(!isValid){
            System.out.println("Строка должна содержать хотябы одну пару открывающихся/закрывающихся тегов");
        }

        return isValid;
    }
}
