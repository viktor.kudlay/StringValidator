import dto.ResultBunch;
import dto.СharactersBunch;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.StringValidator;

/**
 * Created by ghostcky on 04.10.16.
 */
public class MainTest {

    private static StringValidator validator = null;

    @Before
    public void initValidator() {
        validator = new StringValidator(
                new СharactersBunch('{', '}'),
                new СharactersBunch('[', ']'),
                new СharactersBunch('(', ')'));
    }

    @Test
    public void test1() throws NoSuchFieldException {
        System.out.println("\ntest1");
        // пример из задания
        Assert.assertTrue(validate("{{}}[]").isValid());
    }

    @Test
    public void test2() throws NoSuchFieldException {
        System.out.println("\ntest2");
        // пример из задания
        Assert.assertTrue(validate("[{}{}]").isValid());
    }

    @Test
    public void test3() throws NoSuchFieldException {
        System.out.println("\ntest3");
        // пример из задания
        Assert.assertFalse(validate("{[}]").isValid());
    }

    @Test
    public void test4() throws NoSuchFieldException {
        System.out.println("\ntest4");
        // Если присутствуют сторонии символы в тексте, проверка которых не требуется
        Assert.assertTrue(validate("[{(!test)t@est()(te#st)()}tes%t]test^", true).isValid());
    }

    @Test
    public void test5() throws NoSuchFieldException {
        System.out.println("\ntest5");
        // Если присутствуют сторонии символы в тексте, но не передан флаг о их наличии
        Assert.assertFalse(validate("[{(!test)t@est()(te#st)()}tes%t]test^").isValid());
    }

    @Test
    public void test6() throws NoSuchFieldException {
        System.out.println("\ntest6");
        Assert.assertFalse(validate("{{{{[]}}").isValid());
    }

    @Test
    public void test7() throws NoSuchFieldException {
        System.out.println("\ntest7");
        Assert.assertFalse(validate("}{{}]]{{}}").isValid());
    }

    @Test
    public void test8() throws NoSuchFieldException {
        System.out.println("\ntest8");
        Assert.assertFalse(validate("").isValid());
    }

    @Test
    public void test9() throws NoSuchFieldException {
        System.out.println("\ntest9");
        Assert.assertFalse(validate(null).isValid());
    }

    @Test
    public void test10() throws NoSuchFieldException {
        System.out.println("\ntest10");
        Assert.assertFalse(validate("s[df)sdf{").isValid());
    }

    private ResultBunch validate(String str) throws NoSuchFieldException {
        return validate(str, false);
    }

    private ResultBunch validate(String str, boolean ext) throws NoSuchFieldException {
        ResultBunch result = new ResultBunch(false);
        try {
            result = validator.validate(str, ext);
            if(!result.isValid())
                System.out.println("\""+str+"\" is not valid");
            else
                System.out.println(result.getBrackets().toString());
        } catch (NullPointerException | NoSuchFieldException e ) {
            e.printStackTrace(); // Логируем ошибку
            // Не прерываем алгоритм, если считаем что пустое значение не критично.
        }
        return result;
    }
}
